import CommonDependency.dependencies
import sbt.Keys.version

lazy val `dragon-automatic-topic` = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(
    name := "dragon-automatic-topic",
    version := "1.0",
    scalaVersion := "2.11.7",
    mainClass in Compile := Some("com.remarkmedia.automatic.topic.WebServer"),

    libraryDependencies ++= dependencies.elasticsearch,
    libraryDependencies ++= dependencies.json,
    libraryDependencies ++= dependencies.hbase_CDH5,
    libraryDependencies ++= dependencies.logs,
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % "10.0.2" withSources(),
      "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.4" withSources(),
      "com.remarkmedia" %% "dragon-zk" % "2.0" withSources(),
      "com.remarkmedia" %% "dragon-repository-entity" % "2.8" withSources(),
      "io.github.junheng.akka" %% "akka-hbase-protocol" % "0.14" withSources()
    ).map(_.exclude("org.slf4j", "slf4j-log4j12")),

    scalacOptions += "-deprecation",
    scalacOptions += "-feature",

    bashScriptExtraDefines ++= Seq(
      """addJava "-Dfile.encoding=UTF-8""""
    )
  )