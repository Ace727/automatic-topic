package com.remarkmedia.automatic.topic

import akka.actor.{ActorRefFactory, Props}
import akka.http.scaladsl.model.{HttpEntity, _}
import akka.http.scaladsl.server.Directives._
import com.remarkmedia.automatic.topic.datasource.{RedisKey, codis}
import com.remarkmedia.automatic.topic.param.{ParamFormat, ResponseProvider, TopicFollowParam}
import com.remarkmedia.automatic.topic.service.{AutomaticTopicTask, RunningTasks}
import com.remarkmedia.automatic.topic.service.AutomaticTopicTask.FollowTopicMsg
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import com.remarkmedia.automatic.topic.dto.TaskInfo

/**
  * Created by Ace on 17/3/16.
  */
object Routes extends ResponseProvider with ParamFormat {

  def topicRoute(implicit context: ActorRefFactory) = {
    get {
      path("helloworld") {
        parameter("text") { text =>
          println(text)
          complete(HttpResponse(status = StatusCodes.OK, entity = HttpEntity(ContentTypes.`text/plain(UTF-8)`, text)))
        }
      }
    } ~
      post {
        path("automatic" / "test") {
          entity(as[TopicFollowParam]) { followTopic =>
            complete(HttpEntity(ContentTypes.`application/json`, writePrettyJson(followTopic)))
          }
        }
      } ~
      get{
        path("automatic" / "runningTaskList"){
          val runningTasks=RunningTasks.tasks.values.toList.map{actor=>
            TaskInfo(actor.taskId,actor.taskParam)
          }
          complete(HttpEntity(ContentTypes.`application/json`, writePrettyJson(runningTasks)))
        }
      } ~
      post {
        path("automatic" / "clearTopicTask") {
          parameter("trendingId") { trendingId =>
            //清除Topic已经发送post状态
            val key = RedisKey.HAS_SENT_POSTS + trendingId
            codis.delKey(key)
            //清除运行状态的task
            val topicTasksKey = RedisKey.FollowingTopicTasks + trendingId
            codis.delKey(topicTasksKey)
            complete(HttpEntity(ContentTypes.`application/json`, success("success")))
          }
        }
      } ~
      post {
        path("automatic" / "stopTopicTask") {
          parameter("taskId", "trendingId") { (taskId, trendingId) =>
            val topicTaskId = s"topic_follow_task:$taskId-$trendingId-${AutomaticTopicTask.getClass.getSimpleName.replace("$", "")}"
            if (RunningTasks.stopTask(topicTaskId) == 1) {
              complete(HttpEntity(ContentTypes.`application/json`, success("success")))
            } else {
              complete(HttpEntity(ContentTypes.`application/json`, failure(BAD_REQUEST, "job does not exist")))
            }
          }
        }
      } ~
      post {
        path("automatic" / "resetTopic") {
          parameter("trendingId") { trendingId =>
            //清除Topic已经发送post状态
            val key = RedisKey.HAS_SENT_POSTS + trendingId
            codis.delKey(key)
            //停止所有topic相关任务
            RunningTasks.tasks.keys.filter(_.contains(trendingId)).foreach(RunningTasks.stopTask(_))
            complete(HttpEntity(ContentTypes.`application/json`, success("success")))
          }
        }
      } ~
      post {
        path("automatic" / "followTopic") {
          entity(as[TopicFollowParam]) { followTopic =>
            import followTopic._
            val topicTaskId = s"topic_follow_task:$taskId-$trendingId-${AutomaticTopicTask.getClass.getSimpleName.replace("$", "")}"
            val topicTasksKey = RedisKey.FollowingTopicTasks + trendingId
            val entity = codis.sadd(topicTasksKey, topicTaskId).map(_.toInt) collect {
              case 1 =>
                val followTopicMsg = FollowTopicMsg(followEndTime, keepAlive, scrollSize, scrollInterval, trendingId, startTime, endTime, targets.getOrElse(Nil),
                  shouldKeywords.getOrElse(Nil), mustKeywords.getOrElse(Nil), profileIds.getOrElse(Nil), likesCount, commentsCount, imageLevel, videoLevel, textLevel,
                  postType,withDescription.getOrElse(true))
                //创建topic跟踪任务
                context.actorOf(Props(new AutomaticTopicTask(followTopicMsg, topicTaskId)), topicTaskId)
                HttpEntity(ContentTypes.`application/json`, success("topic follow task created success"))
              case 0 =>
                HttpEntity(ContentTypes.`application/json`, failure(BAD_REQUEST, "topic follow task is already exists"))
            }
            complete(entity)
          }
        }
      }
  }
}
