package com.remarkmedia.automatic.topic.param

/**
  * Created by Ace on 17/3/16.
  */
final case class TopicFollowParam(taskId:Int, followEndTime:Long, keepAlive:Int, scrollSize:Int, scrollInterval:Int, trendingId: Long, startTime: Option[Long], endTime: Option[Long],
                                  targets:Option[List[String]],shouldKeywords: Option[List[String]], mustKeywords: Option[List[String]], profileIds: Option[List[String]],
                                  likesCount: Int = 0, commentsCount: Int = 0, imageLevel: Int = 0, videoLevel: Int = 0, textLevel: Int = 0, postType: Option[String] = None,
                                  withDescription:Option[Boolean])
