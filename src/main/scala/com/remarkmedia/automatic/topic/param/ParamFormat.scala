package com.remarkmedia.automatic.topic.param

import spray.json.DefaultJsonProtocol._

/**
  * Created by Ace on 17/3/16.
  */
trait ParamFormat {
  implicit val followTopicFormat = jsonFormat19(TopicFollowParam)
}
