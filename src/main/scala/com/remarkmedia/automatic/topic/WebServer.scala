package com.remarkmedia.automatic.topic

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.routing.RoundRobinPool
import akka.stream.Supervision._
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.remarkmedia.automatic.topic.datasource.HbasePostSearch.{ImageExtEntity, VideoExtEntity}
import com.remarkmedia.automatic.topic.service.TopicSendService
import com.remarkmedia.dragon.zk.ZkDefineFactory
import com.typesafe.config.ConfigFactory
import io.github.junheng.akka.hbase.OHM
import org.slf4j.LoggerFactory

/**
  * Created by Ace on 17/3/16.
  */
object WebServer extends App {
  ZkDefineFactory.load("automatic-topic")

  private val logger = LoggerFactory.getLogger(getClass)

  private val config = ConfigFactory.load()

  implicit val system = ActorSystem("AutomaticTopic")

  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))

  val postInterval = 1

  val postUrl="http://10.1.0.70:9113/api/V2/trendingDetail/batch"
//  val postUrl = "http://sdk.stage.pub.hzvb.kankanapp.com.cn/api/V2/trendingDetail/batch"

  val topicSendService = system.actorOf(RoundRobinPool(3).props(Props(new TopicSendService(postInterval,postUrl))), "topicSendServiceRouter")

  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher
  val decider: Decider = {
    case e: Throwable => {
      logger.error(s"Catch Throwable => ${e.getMessage}", e)
      Supervision.stop
    }
  }
  val api = {
    Routes.topicRoute
  }

  OHM.loadSchemas("com.remarkmedia.dragon.repository.protocol.entity")(system.log)
  OHM.registerSchema(classOf[ImageExtEntity])(system.log)
  OHM.registerSchema(classOf[VideoExtEntity])(system.log)

  Http().bindAndHandle(api, config.getString("http.host"), config.getInt("http.port"))
//  Http().bindAndHandle(api, "localhost", config.getInt("http.port"))
}
