package com.remarkmedia.automatic.topic.datasource

import java.nio.ByteBuffer

import com.remarkmedia.automatic.topic.dto.VideoDetailInfo
import com.remarkmedia.dragon.repository.protocol.entity.Entity
import io.github.junheng.akka.hbase.{CF, CQ, OHM}
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.hbase.client.Get
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}

/**
  * Created by remarkmedia on 17/2/17.
  */
object HbasePostSearch {
  private val logger = LoggerFactory.getLogger(this.getClass)

  implicit val ordering = new Ordering[Option[String]] {
    override def compare(x: Option[String], y: Option[String]): Int = {
      (x, y) match {
        case (Some(a), Some(b)) => a.compareTo(b)
        case (Some(_), None) => -1
        case (None, Some(_)) => 1
        case (None, None) => 0
      }
    }
  }

  def searchImageUrl(postRowkey: Array[Byte]): Option[String] = {
    hbase.scan("image", start = postRowkey)(_.next(5)).map(OHM.fromResult[ImageExtEntity]).sortBy(_.imageType).headOption match {
      case Some(image) if image.originalUrl.nonEmpty => Some(image.originalUrl)
      case _ => None
    }
  }

  def getImageEntity(postRowkey: Array[Byte]): Option[ImageExtEntity] = {
    hbase.scan("image", start = postRowkey)(_.next(5)).map(OHM.fromResult[ImageExtEntity]).sortBy(_.imageType).headOption
  }

  def searchVideoUrl(postRowkey: Array[Byte]): Option[VideoDetailInfo] = {
    Try {
      hbase.crud("video") { table =>
        val result = table.get(new Get(postRowkey))
        if (result.isEmpty) {
          None
        } else {
          val videoEntity = OHM.fromResult[VideoExtEntity](result)

          val coverUrlOption = videoEntity match {
            case video: VideoExtEntity if video.coverUrl.nonEmpty => video.coverUrl
            case video: VideoExtEntity if video.originalCoverUrl.nonEmpty => video.originalCoverUrl
            case _ => None
          }
          val videoUrlOption = videoEntity match {
            case video: VideoExtEntity if video.ossUrl.nonEmpty => video.ossUrl
            case video: VideoExtEntity if video.videoUrl.nonEmpty => video.videoUrl
            case video: VideoExtEntity if video.originalVideoUrl.nonEmpty => Some(video.originalVideoUrl)
            case _ => None
          }

          (coverUrlOption, videoUrlOption) match {
            case (Some(coverUrl), Some(videoUrl)) => Some(VideoDetailInfo(coverUrl, videoUrl, videoEntity.ossWidth, videoEntity.ossHeight))
            case _ => None
          }
        }
      }
    } match {
      case Success(option) => option
      case Failure(e) => logger.error(s"Search video url failed, error=> ${e.getMessage}", e); None
    }
  }

  def postRowKey(id: String, targetId: String, target: String, timestamp: Long): Array[Byte] = {
    postRowKey(id, profileRowKey(targetId, target), timestamp)
  }

  def postRowKey(id: String, profileKey: Array[Byte], timestamp: Long) = {
    val bbf = ByteBuffer.allocate(32)
    bbf.put(profileKey)
    bbf.putLong(Long.MaxValue - timestamp)
    bbf.put(DigestUtils.md5(id).slice(0, 7))
    bbf.array()
  }

  def profileRowKey(targetId: String, target: String) = {
    DigestUtils.md5(targetId + ":" + target)
  }

  @CF(0x41)
  case class ImageExtEntity
  (
    @CQ(0x00) id: String,
    @CQ(0x01) originalUrl: String,
    @CQ(0x02) url: Option[String] = None,
    @CQ(0x03) width: Option[Int] = None,
    @CQ(0x04) height: Option[Int] = None,
    @CQ(0x05) imageType: Option[String] = None,
    @CQ(0x06) ossUrl: Option[String] = None,
    @CQ(0x07) ossWidth: Option[Int] = None,
    @CQ(0x08) ossHeight: Option[Int] = None
  ) extends Entity

  @CF(0x41)
  case class VideoExtEntity
  (
    @CQ(0x00) id: String,
    @CQ(0x01) originalVideoUrl: String,
    @CQ(0x02) originalCoverUrl: Option[String] = None,
    @CQ(0x03) coverUrl: Option[String] = None,
    @CQ(0x04) videoUrl: Option[String] = None,
    @CQ(0x06) ossUrl: Option[String] = None,
    @CQ(0x07) ossWidth: Option[Int] = None,
    @CQ(0x08) ossHeight: Option[Int] = None
  ) extends Entity

}
