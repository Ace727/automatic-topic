package com.remarkmedia.automatic.topic.datasource

/**
  * Created by Ace on 17/3/17.
  */
object RedisKey {
  final val FollowingTopicTasks = "AUTOMATIC_TOPIC_FOLLOWING_TOPICS:"    //跟中ing的专题任务集合
  final val HAS_SENT_POSTS  = "AUTOMATIC_TOPIC_TOPIC_POSTS:"        //已经发送过的postId
}
