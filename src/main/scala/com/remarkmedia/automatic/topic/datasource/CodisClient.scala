package com.remarkmedia.automatic.topic.datasource

import akka.event.slf4j.Logger
import com.wandoulabs.jodis.RoundRobinJedisPool
import redis.clients.jedis.{Jedis, JedisPoolConfig}

class CodisClient(db: String, zk: String, zkTimeout: Int) {
  val log = Logger(getClass.getName)

  val codisPool = {
      val config = new JedisPoolConfig()
      config.setMaxTotal(1024)
      config.setMaxIdle(128)
      config.setMaxWaitMillis(-1)
      config.setBlockWhenExhausted(true)
      RoundRobinJedisPool
        .create()
        .poolConfig(config)
        .curatorClient(zk, zkTimeout)
        .timeoutMs(12000)
        .zkProxyDir(s"/zk/codis/$db/proxy")
        .build()
    }

  def apply[T](process: Jedis => T): Option[T] = {
    try {
      val resource = codisPool.getResource
      try Option(process(resource)) catch {
        case ex: Throwable =>
          log.error("failed in jodis brace", ex)
          None
      } finally {
        resource.close()
      }
    } catch {
      case ex: Throwable =>
        log.error("failed to get jodis resource", ex)
        None
    }
  }
}

object CodisClient {
  def apply(db: String, zk: String, zkTimeout: Int): CodisClient = new CodisClient(db, zk, zkTimeout)

}
