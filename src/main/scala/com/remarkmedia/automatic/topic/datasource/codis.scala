package com.remarkmedia.automatic.topic.datasource

import com.typesafe.config.ConfigFactory

/**
  * Created by Ace on 17/3/17.
  */
object codis {
  private val config = ConfigFactory.load()

  val jodis = CodisClient(config.getString("codis.db"), config.getString("codis.proxy"), 30000)

  def sadd(key: String, value: List[String]) = jodis {
    redis =>
      if(value.nonEmpty){
        redis.sadd(key, value:_*)
      }
  }

  def sadd(key:String,value:String)= jodis {
    redis =>
      redis.sadd(key, value)
  }

  def sismember(key:String,value:String)= jodis{
    redis=>
      redis.sismember(key,value)
  }

  def spop(key:String)=jodis{
    redis=>
      redis.spop(key)
  }

  def srem(key:String,value:String)=jodis{
    redis=>
      redis.srem(key,value)
  }

  def delKey(key:String)=jodis{
    redis=>
      redis.del(key)
  }
}
