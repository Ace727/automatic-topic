package com.remarkmedia.automatic.topic.datasource

import com.typesafe.config.ConfigFactory
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.{HBaseConfiguration, HConstants, TableName}

object hbase {
  private val config = ConfigFactory.load().getConfig("hbase")

  private val connection = {
    val configuration = HBaseConfiguration.create()
    configuration.set("hbase.zookeeper.quorum", config.getString("zookeepers"))
    configuration.set("hbase.zookeeper.property.clientPort", config.getString("zookeepers-port"))
    configuration.set("hbase.client.scanner.timeout.period", "86400000")
    ConnectionFactory.createConnection(configuration)
  }

  def crud[T](tableName: String)(process: (Table) => T) = {
    val table = connection.getTable(TableName.valueOf(tableName))
    table.setWriteBufferSize(1024 * 1024 * 8)
    try process(table) finally {
      table.close()
    }
  }

  def scan[T]
  (tableName: String,
   start: Array[Byte] = HConstants.EMPTY_START_ROW,
   end: Array[Byte] = HConstants.EMPTY_END_ROW
  )(process: (ResultScanner) => T) = {
    val table: Table = connection.getTable(TableName.valueOf(tableName))
    try {
      val _scan = new Scan(start, end)
      _scan.setCaching(100)
      val scanner = table.getScanner(_scan)
      try process(scanner) finally {
        scanner.close()
      }
    } finally {
      table.close()
    }
  }
}