package com.remarkmedia.automatic.topic.datasource


import java.util.Base64

import com.remarkmedia.automatic.topic.service.AutomaticTopicTask.FollowTopicMsg
import com.sksamuel.elastic4s.ElasticDsl.{search, termQuery, _}
import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri, QueryDefinition, SortDefinition}
import com.typesafe.config.ConfigFactory
import org.apache.commons.codec.digest.DigestUtils
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.sort.SortOrder
import org.slf4j.LoggerFactory

import scala.collection.mutable.ListBuffer

/**
  * Created by dengsi on 2017/2/7.
  */
object elasticsearch {
  private val logger = LoggerFactory.getLogger(getClass)

  private val config = ConfigFactory.load()

  private val settings = Settings.settingsBuilder().put("cluster.name", config.getString("elasticsearch.id")).build()

  private val client = ElasticClient.transport(settings, ElasticsearchClientUri(config.getString("elasticsearch.uri")))

  /**
    * 根据条件查询POST
    *
    * @param searchInfo 查询条件
    * @return 满足条件的POST列表
    */
  def searchPostBot(searchInfo: FollowTopicMsg) = {
    import searchInfo._

    val blockQuery: Option[QueryDefinition] = Some(not(termQuery("flags", "block")))
//    val ossQuery: Option[QueryDefinition] = postType match {
//      case Some("text") => None
//      case _ => Some(termQuery("oss", 1))
//    }
    val typeQuery: Option[QueryDefinition] = postType match {
      case Some(tp) => Some(termQuery("type", tp))
      case None => Some(not(termQuery("type", "text")))
    }
    val timeQuery: Option[QueryDefinition] = (startTime, endTime) match {
      case (Some(start), Some(end)) => Some(rangeQuery("timestamp").gte(start).lte(end))
      case (Some(start), None) => Some(rangeQuery("timestamp").gte(start))
      case (None, Some(end)) => Some(rangeQuery("timestamp").lte(end))
      case _ => None
    }
    val shouldQuery: Option[QueryDefinition] = shouldKeywords match {
      case Nil => None
      case keywords => Some(should(keywords.map { kw =>
        termQuery("content", kw.toLowerCase)
      }))
    }
    val mustQuery: Option[QueryDefinition] = mustKeywords match {
      case Nil => None
      case keywords => Some(must(keywords.map { kw =>
        termQuery("content", kw.toLowerCase)
      }))
    }
    val targetQuery: Option[QueryDefinition] = targets match {
      case Nil => None
      case targetList => Some(should(termsQuery("target", targetList: _*)))
    }
    val profileQuery: Option[QueryDefinition] = profileIds match {
      case Nil => None
      case profileList => Some(should(termsQuery("profileId", profileList.map(encodeProfileId): _*)))
    }
    val likeQuery: Option[QueryDefinition] = Some(rangeQuery("likesCount").gte(likesCount))
    val commentQuery: Option[QueryDefinition] = Some(rangeQuery("commentsCount").gte(commentsCount))

    val queryOptionList = List(blockQuery, typeQuery, timeQuery, shouldQuery, mustQuery, targetQuery, profileQuery, likeQuery, commentQuery)
    val queryList = queryOptionList.foldRight(List.empty[QueryDefinition]) { case (option, list) =>
      option match {
        case Some(query) => query :: list
        case None => list
      }
    }

    val queryDefine = bool(must(queryList))
    logger.info(s"Trending follow query: ${queryDefine.builder.toString}")
    val sortDefine = field sort "likesCount" order SortOrder.DESC
    scrollQuery("post_*", queryDefine, sortDefine, s"${keepAlive}m", scrollSize)
  }

  /**
    * 滚动查询满足所有条件的记录
    *
    * @param index       index
    * @param queryDefine 查询条件
    * @param sortDefine  排序条件
    * @param scrollAlive 存活时间
    * @param scrollSize  滚动单页长度
    * @return 满足条件的所有记录
    */
  def scrollQuery(index: String, queryDefine: QueryDefinition, sortDefine: SortDefinition, scrollAlive: String, scrollSize: Int) = {
    val hitList = new ListBuffer[SearchHit]
    var resp = client.execute {
      search in index query queryDefine scroll scrollAlive size scrollSize sort sortDefine
    }.await
    hitList ++= resp.getHits.getHits
    while (!resp.isEmpty) {
      resp = client.execute {
        searchScroll(resp.getScrollId).keepAlive(scrollAlive)
      }.await
      hitList ++= resp.getHits.getHits
    }
    hitList.toList
  }

  def encodeProfileId(profileId: String): String = Base64.getEncoder.encodeToString(DigestUtils.md5(profileId))
}