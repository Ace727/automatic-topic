package com.remarkmedia.automatic.topic.util

import java.io.{BufferedReader, InputStream, InputStreamReader}
import java.net.{SocketTimeoutException, URLDecoder}

import org.apache.commons.httpclient.methods.{GetMethod, PostMethod}
import org.apache.commons.httpclient.params.{HttpConnectionManagerParams, HttpMethodParams}
import org.apache.commons.httpclient.{HttpClient, HttpConnectionManager, MultiThreadedHttpConnectionManager}
import org.apache.commons.io.IOUtils
import org.apache.commons.lang.StringUtils._

object HttpUtils {

  private lazy val client: HttpClient = {
    val httpConnectionManager: HttpConnectionManager = new MultiThreadedHttpConnectionManager
    val params: HttpConnectionManagerParams = httpConnectionManager.getParams
    params.setConnectionTimeout(5000)
    params.setSoTimeout(5000)
    params.setDefaultMaxConnectionsPerHost(64)
    params.setMaxTotalConnections(512)
    new HttpClient(httpConnectionManager)
  }

  def httpGet(urlParams: String): String = {
    val getMethod = new GetMethod(urlParams)
    val (statusCode, response) = try {
      getMethod.getParams.setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8")
      client.executeMethod(getMethod) -> getMethod.getResponseBodyAsString
    } finally {
      getMethod.releaseConnection()
    }
    if (2 != statusCode / 100 || isEmpty(response)) {
      throw new IllegalStateException(s"fail to http get '$urlParams' $statusCode $response")
    }
    response
  }

  def httpDownload(urlParams: String, process: (InputStream) => Unit): Unit = {
    val getMethod = new GetMethod(urlParams)
    val statusCode = client.executeMethod(getMethod)
    if (2 != statusCode / 100) {
      throw new IllegalStateException(s"fail to http download '$urlParams' $statusCode")
    }
    val inputStream = getMethod.getResponseBodyAsStream
    try {
      process(inputStream)
    } catch {
      case t: Throwable =>
        throw new IllegalStateException(s"fail to http download '$urlParams'", t)
    } finally {
      IOUtils.closeQuietly(inputStream)
      getMethod.releaseConnection()
    }
  }



  def httpPost(jsonstr: String, url: String) ={
    val postMethod = new PostMethod(url)
    postMethod.addRequestHeader( "Content-Type","application/json" )
    postMethod.getParams().setParameter( HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8" )
    postMethod.setRequestBody( jsonstr )
    try {
      val statusCode=client.executeMethod(postMethod)
      val inputStream = postMethod.getResponseBodyAsStream
      val bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"))
      val response = new StringBuilder()
      var line = ""
      do{
        line = bufferedReader.readLine()
        response.append(line)
      }while(line!=null)
      PostResponse(statusCode,response.toString())
    } finally {
      postMethod.releaseConnection()
    }
  }

  def httpPatch(jsonstr: String, url :String): Unit ={
    val patchMethod = new PatchMethod(url)
    patchMethod.setRequestHeader("Content-Type","application/json")
    patchMethod.setRequestBody(jsonstr)
    val (statusCode,response) = try{
      client.executeMethod(patchMethod)-> patchMethod.getResponseBodyAsString
    }finally {
      patchMethod.releaseConnection()
    }
    println("response is: "+response)
    if (2 != statusCode / 100) {
      println("statusCode is:"+statusCode)
      throw new IllegalStateException(s"fail to http patch '$url' $statusCode")
    }
    println("patch success!")
  }



  def urlDecode(urlEncodedString: String): String = {
    URLDecoder.decode(urlEncodedString, "UTF-8")
  }

}
