package com.remarkmedia.automatic.topic.util

/**
  * Created by Ace on 17/4/12.
  */
case class PostResponse(statusCode:Int,response:String)

case class ResponseEntity(meta:ResponseMeta,content:ResponseContent)

case class ResponseMeta(statusCode:Int,description:String)

case class ResponseContent(message:String,status:Option[String])