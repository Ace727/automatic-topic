package com.remarkmedia.automatic.topic.util

import java.nio.ByteBuffer
import java.security.MessageDigest
import java.util.Base64

import com.google.protobuf.ByteString
import org.apache.commons.codec.digest.DigestUtils


object IdGenUtils {
  var workIdSeq = 0L

  def docId(hbaseRowKey: Array[Byte]): String = {
    val buffer = ByteBuffer.wrap(hbaseRowKey)
    val profileIdMd5 = new Array[Byte](16)
    val timestamp = new Array[Byte](8)
    val deduplicate = new Array[Byte](8)
    buffer.get(profileIdMd5)
    buffer.get(timestamp)
    buffer.get(deduplicate)
    Base64.getEncoder.encodeToString(timestamp ++ profileIdMd5 ++ deduplicate)
  }

  def encodeRowKey(rowKey: Array[Byte]): String = {
    Base64.getEncoder.encodeToString(rowKey)
  }

  def docId2RowKey(docId: String): Array[Byte] = {
    val docIdBytes = Base64.getDecoder.decode(docId)
    val buffer = ByteBuffer.wrap(docIdBytes)
    val profileIdMd5 = new Array[Byte](16)
    val timestamp = new Array[Byte](8)
    val deduplicate = new Array[Byte](8)
    buffer.get(timestamp)
    buffer.get(profileIdMd5)
    buffer.get(deduplicate)
    profileIdMd5 ++ timestamp ++ deduplicate
  }

  def esId(id: Array[Byte]): String = {
    val bytes = ByteBuffer.allocate(16)
    bytes.put(id)
    bytes.position(8)
    java.lang.Long.toString(bytes.getLong, Character.MAX_RADIX)
  }

  def generatePostRowKey(id: String, target: String, targetId: String, timestamp: Long) = {
    val bbf = ByteBuffer.allocate(32)
    bbf.put(hashPostId(targetId, target))
    bbf.putLong(Long.MaxValue - timestamp)
    bbf.put(DigestUtils.md5(id).slice(0, 7))
    bbf.array()
  }

  def profileRowKey(profileId: String) = {
    DigestUtils.md5(profileId)
  }

  def postRowKey(id: String, profileKey: Array[Byte], timestamp: Long) = {
    val bbf = ByteBuffer.allocate(32)
    bbf.put(profileKey)
    bbf.putLong(Long.MaxValue - timestamp)
    bbf.put(DigestUtils.md5(id).slice(0, 7))
    bbf.array()
  }


  def hashPostId(targetId: String, target: String) = DigestUtils.md5(s"$targetId:$target")

  def rowKey(esId: String): Array[Byte] = {
    sequenceId2Rowkey(java.lang.Long.parseLong(esId, Character.MAX_RADIX))
  }

  def sequenceIdBytes(id: Array[Byte]) = {
    val bytes = ByteBuffer.allocate(16)
    bytes.put(id)
    bytes.position(8)
    val result = new Array[Byte](8)
    bytes.get(result).array()
  }

  def sequenceId2Rowkey(sid: Long) = {
    val bytes = ByteBuffer.allocate(16)
    bytes.put(0.toByte)
    bytes.put(hash(sid), 0, 7)
    bytes.put(ByteBuffer.allocate(8).putLong(sid).array())
    bytes.array()
  }

  def sequenceId(id: Array[Byte]) = {
    val bytes = ByteBuffer.allocate(16)
    bytes.put(id)
    bytes.position(8)
    val result = new Array[Byte](8)
    bytes.get(result)
    new String(result, "ASCII")
  }


  def getLong(id: Array[Byte]): Long = {
    val bytes = ByteBuffer.allocate(16)
    bytes.put(id)
    bytes.position(0)
    val seqBytes = ByteBuffer.allocate(8).put(sequenceId(id).getBytes("ASCII"))
    seqBytes.flip()
    seqBytes.getLong
  }

  def toStrId(id: Array[Byte]) = {
    val bytes = ByteBuffer.allocate(16)
    bytes.put(id)
    bytes.position(0)
    val seqBytes = ByteBuffer.allocate(8).put(sequenceId(id).getBytes("ASCII"))
    seqBytes.flip()
    seqBytes.getLong
  }

  private def hash(value: Long) = {
    val digest = MessageDigest.getInstance("MD5")
    digest.digest(ByteBuffer.allocate(8).putLong(value).array())
  }

  def userId2ByteString(userId: String): ByteString = ByteString.copyFrom(DigestUtils.md5(userId))

  def profileId2Md5String(profileId:String):String = Base64.getEncoder.encodeToString(DigestUtils.md5(profileId))

  def userId2ByteStringOfUTF8(userId: String): ByteString = ByteString.copyFromUtf8(userId)

}
