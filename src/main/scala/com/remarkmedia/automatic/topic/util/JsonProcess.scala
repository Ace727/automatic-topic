package com.remarkmedia.automatic.topic.util

import org.json4s.Extraction
import org.json4s.jackson.JsonMethods


trait JsonProcess {

	private implicit lazy val formats = org.json4s.DefaultFormats

	def writePrettyJson[A <: AnyRef](a: A):String = {
		JsonMethods.pretty(JsonMethods.render(Extraction.decompose(a)(formats)))
	}

	def parseJson[A](s:String)(implicit mf:Manifest[A]): Option[A] = {
		JsonMethods.parse(s).extractOpt[A](formats, mf)
	}
}
