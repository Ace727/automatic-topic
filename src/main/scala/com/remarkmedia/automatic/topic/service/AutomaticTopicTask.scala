package com.remarkmedia.automatic.topic.service

import java.io.PrintWriter

import akka.actor.Actor
import com.remarkmedia.automatic.topic.WebServer
import com.remarkmedia.automatic.topic.datasource.{HbasePostSearch, RedisKey, codis, elasticsearch}
import com.remarkmedia.automatic.topic.dto.{BatchTrendingDetailMessage, Image, ProfileInfo, TrendingDetailInfo}
import com.remarkmedia.automatic.topic.service.AutomaticTopicTask.{FollowTopicMsg, StopMsg}
import com.remarkmedia.automatic.topic.service.TopicSendService.SendTopicMsg
import com.remarkmedia.automatic.topic.util.JsonUtils
import com.remarkmedia.automatic.topic.util.JsonUtils.formats
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import AutomaticTopicTask._

/**
  * Created by Ace on 17/3/16.
  */
class AutomaticTopicTask(searchInfo: FollowTopicMsg, topicTaskId: String) extends Actor {

  private val log = LoggerFactory.getLogger(getClass)

  val taskParam = searchInfo
  val taskId = topicTaskId

  implicit var executionContext: ExecutionContextExecutor = context.dispatcher

  override def preStart(): Unit = {
    val initDelay = 5.seconds
    log.info("searchInfo.scrollInterval:" + searchInfo.scrollInterval)
    //发送消息
    context.system.scheduler.schedule(initDelay, searchInfo.scrollInterval.minutes, self, searchInfo)
    RunningTasks.tasks.put(topicTaskId, this)
  }

  override def receive: Receive = {
    case searchInfo: FollowTopicMsg =>
      log.info(s"recieve a FollowTopicMsg:$searchInfo")
      //判断是否结束当前任务
      if (System.currentTimeMillis() > searchInfo.followEndTime) {
        log.info(s" task:$self end !!!!")
        end()
      } else {
        searchResult2Json(AutomaticTopicTask.jsonFile, searchInfo, WebServer.postUrl)
      }
    case StopMsg =>
      log.info(s"s recieve a StopMsg topicTaskId:$topicTaskId")
      end()

  }

  private def end() = {
    context.stop(self)
    val topicTasksKey = RedisKey.FollowingTopicTasks + searchInfo.trendingId
    //移除当前正在跟踪topic任务集合
    codis.srem(topicTasksKey, topicTaskId)
    RunningTasks.tasks.remove(topicTaskId)
  }

  def getPlatformId(target: String): String = {
    target match {
      case "unknown" => "-1"
      case "weibo" => "1"
      case "weixin" => "2"
      case "renren" => "3"
      case "instagram" => "4"
      case "qqzone" => "5"
      case "cicada_travel" => "6"
      case "yelp" => "7"
      case "dianping" => "8"
      case "facebook" => "9"
      case "pinterest" => "10"
      case "twitter" => "11"
      case "tumblr" => "12"
      case "snapchat" => "13"
      case "foursquare" => "14"
      case "groupon" => "15"
      case "livingsocial" => "16"
      case "kakao" => "17"
      case "kankan" => "18"
      case "youtube" => "19"
      case "youtube_playlist"=>"20"
      case "vlive"=>"21"
      case "naver"=>"22"
      case "periscope"=>"23"
      case _ => "0"
    }
  }

  def postResourceIdDecode(encodeId: String, platform: String): String = {
    platform match {
      case "instagram" => encodeId.split(":")(0).split("_")(0)
      case _ => encodeId.split(":")(0)
    }
  }

  def searchResult2Json(jsonFile: String, searchInfo: FollowTopicMsg, postUrl: String): Unit = {
    val key = RedisKey.HAS_SENT_POSTS + searchInfo.trendingId
    val result = elasticsearch.searchPostBot(searchInfo)
    log.info(s"topicTaskId:$topicTaskId,es query result:${result.size}")
    val hitsIter = result.iterator
    var trandArray = new ArrayBuffer[TrendingDetailInfo]()
    while (hitsIter.hasNext) {
      //获取profileInfo
      try {
        val hit = hitsIter.next()
        val post_id = hit.id()
        val sourceMap = hit.getSource
        val target = sourceMap.get("target").toString
        val postId = postResourceIdDecode(sourceMap.get("id").toString, target)
        val targetId = sourceMap.get("targetId").toString
        val nickname = sourceMap.get("targetName").toString
        val avatar = sourceMap.get("targetAvatar").toString
        val profileInfo = new ProfileInfo(nickname, avatar, targetId, getPlatformId(target))
        val postType = sourceMap.get("type").toString
        val timestamp = sourceMap.get("timestamp").toString.toLong
        val oss = String.valueOf(sourceMap.get("oss"))
        val content = if (searchInfo.withDescription) {
          sourceMap.get("content").toString
        } else {
          ""
        }
        val likescount = sourceMap.get("likesCount").toString.toInt
        val commentscount = sourceMap.get("commentsCount").toString.toInt
        val postRowkey = HbasePostSearch.postRowKey(sourceMap.get("id").toString, targetId, target, timestamp)
        //postType 等于image
        if (postType.equals("image") && likescount >= imageLevelMap.getOrElse(searchInfo.imageLevel, 0)) {
          val imageOp = HbasePostSearch.getImageEntity(postRowkey)
          if (imageOp.nonEmpty) {
            if(imageOp.get.ossUrl.nonEmpty){
              if (!codis.sismember(key, postId).map(_.booleanValue).get) {
                val image = Some(Image(imageOp.get.originalUrl, imageOp.get.ossUrl, imageOp.get.ossWidth, imageOp.get.ossHeight))
                val trendingDetailInfo = new TrendingDetailInfo(searchInfo.trendingId, postId, timestamp, profileInfo, postType, content, image, None, likeCount = likescount, commentCount = commentscount)
                trandArray.append(trendingDetailInfo)
              }
            }
          }
        } else if (postType.equals("video") && likescount >= videoLevelMap.getOrElse(searchInfo.videoLevel, 0)) {
          //postType 等于video 获取videoDetailInfo
          val videoDetailInfo = HbasePostSearch.searchVideoUrl(postRowkey)
          if (videoDetailInfo.nonEmpty) {
            val ossFilter = if(target=="weibo"){
              true
            }else{
              isCDNOrOssUrl(videoDetailInfo.get.videoUrl)
            }
            if(ossFilter){
              if (!codis.sismember(key, postId).map(_.booleanValue).get) {
                val trendingDetailInfo = new TrendingDetailInfo(searchInfo.trendingId, postId, timestamp, profileInfo, postType, content, None, videoDetailInfo, likeCount = likescount, commentCount = commentscount)
                trandArray.append(trendingDetailInfo)
              }
            }
          }
        }
//        else if (postType.equals("text") && likescount >= textLevelMap.getOrElse(searchInfo.textLevel, 0)) {
//          if (!codis.sismember(key, postId).map(_.booleanValue).get) {
//            val trendingDetailInfo = new TrendingDetailInfo(searchInfo.trendingId, postId, timestamp, profileInfo, postType, content, None, None, likeCount = likescount, commentCount = commentscount)
//            trandArray.append(trendingDetailInfo)
//          }
//        }
      } catch {
        case e: Exception =>
          log.error(e.getMessage)
      }
      if (trandArray.size == searchInfo.scrollSize) {
        val pw_json = new PrintWriter(jsonFile)
        val trandList = trandArray.toList
        val batchTrendingDetailMessage = new BatchTrendingDetailMessage(trandList)
        val jsonStr = JsonUtils.toJson(batchTrendingDetailMessage, false)
        val postIds = trandList.map(_.postId)
        pw_json.write(jsonStr + "\n")
        pw_json.close()
        WebServer.topicSendService ! SendTopicMsg(searchInfo.trendingId, jsonStr, key, postIds)
        trandArray.clear()
      }
    }
    val trandList = trandArray.toList
    log.info(s"topicTaskId:$topicTaskId trendingDetailList:${trandList.size}")
    if(trandList.size>0){
      val pw_json = new PrintWriter(jsonFile)
      val postIds = trandList.map(_.postId)
      val batchTrendingDetailMessage = new BatchTrendingDetailMessage(trandList)
      val jsonStr = JsonUtils.toJson(batchTrendingDetailMessage, false)
      pw_json.write(jsonStr + "\n")
      pw_json.close()
      pw_json.flush()
      WebServer.topicSendService ! SendTopicMsg(searchInfo.trendingId, jsonStr, key, postIds)
    }
  }
  private def isCDNOrOssUrl(url:String)={
    url.contains("rm-avatar") ||
      url.contains("rm-dragon") ||
      url.contains("avator-cdn") ||
      url.contains("rm-dragon-cdn") ||
      url.contains("rd-images")
  }
}

object AutomaticTopicTask {
  val imageLevelMap = Map(1 -> 5, 2 -> 100, 3 -> 500)
  val videoLevelMap = Map(1 -> 5, 2 -> 100, 3 -> 500)
  val textLevelMap = Map(1 -> 5, 2 -> 100, 3 -> 500)

  final val batchSize = 20
  final val jsonFile = "trends.json"

  case class FollowTopicMsg(followEndTime: Long, keepAlive: Int, scrollSize: Int, scrollInterval: Int, trendingId: Long, startTime: Option[Long], endTime: Option[Long],
                            targets: List[String], shouldKeywords: List[String] = Nil, mustKeywords: List[String] = Nil, profileIds: List[String] = Nil,
                            likesCount: Int = 0, commentsCount: Int = 0, imageLevel: Int = 0, videoLevel: Int = 0, textLevel: Int = 0, postType: Option[String] = None,
                            withDescription: Boolean)

  case object StopMsg

}
