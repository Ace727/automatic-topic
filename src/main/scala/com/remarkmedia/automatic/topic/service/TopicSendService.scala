package com.remarkmedia.automatic.topic.service


import akka.actor.Actor
import com.remarkmedia.automatic.topic.datasource.codis
import com.remarkmedia.automatic.topic.service.TopicSendService.SendTopicMsg
import com.remarkmedia.automatic.topic.util.{HttpUtils, JsonProcess, ResponseEntity}
import org.slf4j.LoggerFactory

/**
  * Created by Ace on 17/3/23.
  */
class TopicSendService(postInterval:Int,postUrl:String) extends Actor with JsonProcess{
  private val log = LoggerFactory.getLogger(getClass)
  override def receive: Receive = {
    case SendTopicMsg(trendingId,jsonData,key,postIds)=>
      log.info(s"recieve a SendTopicMsg trendingId:$trendingId,jsonData:$jsonData")
      try{
        val postResponse = HttpUtils.httpPost(jsonData,postUrl)
        log.info("postResponse:"+postResponse)
        if(postResponse.statusCode==200){
          parseJson[ResponseEntity](postResponse.response).map{responseEntity=>
            if(responseEntity.content.status.getOrElse("fail")=="success"){
              if(postIds.nonEmpty){
                codis.sadd(key,postIds)
              }
            }
          }
        }
      }catch{
        case e:Exception=>
          log.error(e.getMessage)
      }
  }
}
object TopicSendService{
  case class SendTopicMsg(trendingId:Long,jsonData:String,key:String,postIds:List[String])
}
