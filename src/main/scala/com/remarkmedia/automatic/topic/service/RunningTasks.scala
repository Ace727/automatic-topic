package com.remarkmedia.automatic.topic.service

import com.remarkmedia.automatic.topic.service.AutomaticTopicTask.StopMsg

/**
  * Created by Ace on 17/3/29.
  */
object RunningTasks {
  val tasks = new scala.collection.mutable.HashMap[String,AutomaticTopicTask]
  def stopTask(topicTaskId:String)={
    tasks.get(topicTaskId).map{task=>
      task.self ! StopMsg
      1
    }.getOrElse(0)
  }
}
