package com.remarkmedia.automatic.topic.service

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable}
import com.remarkmedia.automatic.topic.service.Task.JobTimeout

import scala.concurrent.duration._

abstract class Task(receipt: ActorRef, timeout: FiniteDuration = 5 seconds) extends Actor with ActorLogging {

  import context.dispatcher

  protected var whenTimeout: Option[Cancellable] = None

  protected var startedTime = 0L

  override def preStart() = {
    super.preStart()
    startedTime = System.nanoTime()
    whenTimeout = Some(context.system.scheduler.scheduleOnce(timeout, self, JobTimeout))
  }

  override def postStop() = {
    super.postStop()
    if (cost > 500) {
      log.warning(s"slow job, cost $cost ms")
    } else {
      log.debug(s"was finished, cost $cost ms")
    }
    whenTimeout.collect { case c => c.cancel() }
  }

  protected def cost: Long = {
    (System.nanoTime() - startedTime) / 1000 / 1000
  }

  override def receive: Receive = process orElse {
    case JobTimeout =>
      context.stop(self)
  }

  protected def end() = {
    context.stop(self)
  }

  protected def process: Receive
}

object Task {

  case object JobTimeout

}
