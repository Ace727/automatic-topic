package com.remarkmedia.automatic.topic.dto

abstract class ServiceMsg

case class BatchTrendingDetailMessage(data: List[TrendingDetailInfo]) extends ServiceMsg

case class TrendingDetailInfo(subCategoryId: Long, postId: String, timestamp: Long, profile: ProfileInfo, postType: String, content: String,
                              image: Option[Image], video: Option[VideoDetailInfo], likeCount: Int, commentCount: Int)

case class Image(originalUrl:String,ossUrl:Option[String],width:Option[Int],height:Option[Int])

case class ProfileInfo(nickname: String, avatar: String, targetId: String, platform: String)

case class VideoDetailInfo(coverUrl: String, videoUrl: String,width:Option[Int],height:Option[Int])