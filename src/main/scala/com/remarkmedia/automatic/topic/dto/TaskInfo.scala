package com.remarkmedia.automatic.topic.dto

import com.remarkmedia.automatic.topic.service.AutomaticTopicTask.FollowTopicMsg

/**
  * Created by Ace on 17/4/12.
  */
case class TaskInfo(taskId:String,taskParam:FollowTopicMsg)
